package com.epam.javaxml.initialization;

public enum Constants {
    XML_PATH("src/main/resources/touristVouchers.xml"),
    SCHEMA_PATH("src/main/resources/touristVouchersSchema.xsd"),
    PARSED_DOM_SET_PATH("parsed/parsed-dom-set.txt"),
    PARSED_SAX_SET_PATH("parsed/parsed-sax-set.txt"),
    PARSED_STAX_SET_PATH("parsed/parsed-stax-set.txt"),
    STAX("stax"),
    SAX("sax"),
    DOM("dom");
    private String value;

    Constants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
