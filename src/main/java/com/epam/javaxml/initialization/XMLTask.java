package com.epam.javaxml.initialization;

import com.epam.javaxml.parser.AbstractTouristVouchersBuilder;
import com.epam.javaxml.parser.TouristVoucherBuilderFactory;
import com.epam.javaxml.validator.XMLvalidator;

public class XMLTask {
    public static void main(String[] args) {
        // validating xml file against xsd schema
        XMLvalidator xmLvalidator = new XMLvalidator(Constants.XML_PATH.getValue(), Constants.SCHEMA_PATH.getValue());
        if (xmLvalidator.validateXML()) {
            // creating builder factory
            TouristVoucherBuilderFactory voucherFactory = new TouristVoucherBuilderFactory();
            // creating dom parser
            AbstractTouristVouchersBuilder builder = voucherFactory.createTouristVoucherBuilder(Constants.DOM.getValue());
            // parsing xml file
            builder.buildSetTouristVouchers(Constants.XML_PATH.getValue());
            // writing parsing results to file
            builder.writeParsedSetToFile(Constants.PARSED_DOM_SET_PATH.getValue());
            // parsing using SAX parser
            builder = voucherFactory.createTouristVoucherBuilder(Constants.SAX.getValue());
            builder.buildSetTouristVouchers(Constants.XML_PATH.getValue());
            builder.writeParsedSetToFile(Constants.PARSED_SAX_SET_PATH.getValue());
            // parsing using StAX parser
            builder = voucherFactory.createTouristVoucherBuilder(Constants.STAX.getValue());
            builder.buildSetTouristVouchers(Constants.XML_PATH.getValue());
            builder.writeParsedSetToFile(Constants.PARSED_STAX_SET_PATH.getValue());
        }
    }
}
