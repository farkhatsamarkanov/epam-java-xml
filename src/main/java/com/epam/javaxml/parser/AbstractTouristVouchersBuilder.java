package com.epam.javaxml.parser;

import com.epam.javaxml.entity.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public abstract class AbstractTouristVouchersBuilder {
    protected Set<Voucher> touristVouchers;
    protected final static Logger LOG = LogManager.getLogger("ParserBuilderLogger");

    public AbstractTouristVouchersBuilder() {
    }

    public AbstractTouristVouchersBuilder(Set<Voucher> touristVouchers) {
        this.touristVouchers = touristVouchers;
    }

    // getter
    public Set<Voucher> getTouristVouchers() {
        return touristVouchers;
    }

    // method to build set of Voucher objects
    abstract public void buildSetTouristVouchers(String xmlPath);

    // method to write parsing results to file
    public void writeParsedSetToFile(String fileName) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write("\nTouristVouchers set parsed in " + this.getClass().getSimpleName() + ":\n");
            for (Voucher voucher : getTouristVouchers()) {
                writer.write(String.valueOf(voucher));
            }
            LOG.info(this.getClass().getSimpleName() + " parsing results witten to: " + fileName);
        } catch (IOException e) {
            LOG.error(e);
        } finally {
            try {
                assert writer != null;
                writer.close();
            } catch (IOException e) {
                LOG.error("Failed to close buffered writer " + e);
            }
        }
    }
}
