package com.epam.javaxml.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TouristVoucherBuilderFactory {
    private enum ParserType {
        SAX, STAX, DOM
    }

    private final static Logger LOG = LogManager.getLogger(TouristVoucherBuilderFactory.class);

    public AbstractTouristVouchersBuilder createTouristVoucherBuilder(String parserType) {
        ParserType type = ParserType.valueOf(parserType.toUpperCase());
        switch (type) {
            case SAX:
                LOG.info("SAX parser is created");
                return new TouristVouchersSAXBuilder();
            case STAX:
                LOG.info("StAX parser is created");
                return new TouristVouchersStAXBuilder();
            case DOM:
                LOG.info("DOM parser is created");
                return new TouristVouchersDOMBuilder();
            default:
                throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
        }
    }
}
