package com.epam.javaxml.parser;

import com.epam.javaxml.entity.HotelCharacteristic;
import com.epam.javaxml.entity.HotelIncludedVoucher;
import com.epam.javaxml.entity.TransportIncludedVoucher;
import com.epam.javaxml.entity.Voucher;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

public class TouristVouchersDOMBuilder extends AbstractTouristVouchersBuilder {

    private DocumentBuilder documentBuilder;

    TouristVouchersDOMBuilder() {
        this(new HashSet<Voucher>());
    }

    private TouristVouchersDOMBuilder(Set<Voucher> touristVouchers) {
        super(touristVouchers);
        // configuring parser
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.error("Parser configuration error: " + e);
        }

    }

    // getter
    @Override
    public Set<Voucher> getTouristVouchers() {
        return touristVouchers;
    }

    // method to build set of Voucher objects
    @Override
    public void buildSetTouristVouchers(String xmlPath) {
        Document doc;
        try {
            doc = documentBuilder.parse(xmlPath);
            Element root = doc.getDocumentElement();
            // looking for all xml elements with name "hotelIncludedVoucher"
            NodeList vouchersList = root.getElementsByTagName("hotelIncludedVoucher");
            for (int i = 0; i < vouchersList.getLength(); i++) {
                Element voucherElement = (Element) vouchersList.item(i);
                // creating Voucher object from xml element
                Voucher voucher = buildTouristVoucher("hotelIncludedVoucher", voucherElement);
                // adding Voucher object to set
                touristVouchers.add(voucher);
            }
            // looking for all xml elements with name "transportIncludedVoucher"
            vouchersList = root.getElementsByTagName("transportIncludedVoucher");
            for (int i = 0; i < vouchersList.getLength(); i++) {
                Element voucherElement = (Element) vouchersList.item(i);
                Voucher voucher = buildTouristVoucher("transportIncludedVoucher", voucherElement);
                touristVouchers.add(voucher);
            }
            LOG.info("File " + xmlPath + " parsed successfully using DOM parser");
        } catch (SAXException e) {
            LOG.error("File error or I/O error: " + e);
        } catch (IOException e) {
            LOG.error("Parsing failure: " + e);
        }
    }

    // method to parse Voucher object
    private Voucher buildTouristVoucher(String voucherKind, Element touristVoucherElement) {
        Voucher voucher = null;
        if (voucherKind.equals("hotelIncludedVoucher")) {
            voucher = new HotelIncludedVoucher();
            // setting xml elements to HotelCharacteristic object
            HotelCharacteristic hotelCharacteristic = new HotelCharacteristic();
            Element hotelCharacteristicElement = (Element) touristVoucherElement.getElementsByTagName("hotelCharacteristics").item(0);
            hotelCharacteristic.setFood(getElementTextContent(hotelCharacteristicElement, "food"));
            hotelCharacteristic.setNumberOfStars(new BigInteger(getElementTextContent(hotelCharacteristicElement, "numberOfStars")));
            hotelCharacteristic.setRoomCapacity(new BigInteger(getElementTextContent(hotelCharacteristicElement, "roomCapacity")));
            hotelCharacteristic.setIsTvPresent(Boolean.parseBoolean(getElementTextContent(hotelCharacteristicElement, "isTvPresent")));
            hotelCharacteristic.setIsAirConditionerPresent(Boolean.parseBoolean(getElementTextContent(hotelCharacteristicElement, "isAirConditionerPresent")));
            // setting HotelCharacteristic object to Voucher object
            ((HotelIncludedVoucher) voucher).setHotelCharacteristics(hotelCharacteristic);
        } else if (voucherKind.equals("transportIncludedVoucher")) {
            voucher = new TransportIncludedVoucher();
            ((TransportIncludedVoucher) voucher).setTransport(getElementTextContent(touristVoucherElement, "transport"));
        }
        // setting xml attributes and elements to Voucher object
        if (voucher != null) {
            voucher.setId(touristVoucherElement.getAttribute("id"));
            voucher.setVoucherType(touristVoucherElement.getAttribute("voucherType"));
            voucher.setCost(Double.parseDouble(getElementTextContent(touristVoucherElement, "cost")));
            voucher.setNumberOfDays(new BigInteger(getElementTextContent(touristVoucherElement, "numberOfDays")));
            voucher.setCountry(getElementTextContent(touristVoucherElement, "country"));
        }
        return voucher;
    }

    private static String getElementTextContent(Element element, String elementName) {
        return element.getElementsByTagName(elementName).item(0).getTextContent();
    }


}
