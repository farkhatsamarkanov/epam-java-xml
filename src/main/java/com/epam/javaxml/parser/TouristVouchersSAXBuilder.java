package com.epam.javaxml.parser;

import com.epam.javaxml.entity.Voucher;
import com.epam.javaxml.parser.helper.TouristVoucherHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

// class to parse xml file using SAX parser
public class TouristVouchersSAXBuilder extends AbstractTouristVouchersBuilder {

    private TouristVoucherHandler touristVoucherHandler;
    private XMLReader reader;

    TouristVouchersSAXBuilder() {
        this(new HashSet<Voucher>());
    }
    // configuring SAX parser
    private TouristVouchersSAXBuilder(Set<Voucher> touristVouchers) {
        super(touristVouchers);
        touristVoucherHandler = new TouristVoucherHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(touristVoucherHandler);
        } catch (SAXException e) {
            LOG.error("SAX parser error: " + e);
        }
    }

    @Override
    public Set<Voucher> getTouristVouchers() {
        return touristVouchers;
    }

    // building set of tourist vouchers
    @Override
    public void buildSetTouristVouchers(String xmlPath) {
        try {
            reader.parse(xmlPath);
            LOG.info("File " + xmlPath + " parsed successfully using SAX parser");
        } catch (SAXException e) {
            LOG.error("Parsing failure: " + e);
        } catch (IOException e) {
            LOG.error("File error or I/O error: " + e);
        }
        touristVouchers = touristVoucherHandler.getTouristVouchers();
    }
}
