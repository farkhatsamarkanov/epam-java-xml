package com.epam.javaxml.parser;

import com.epam.javaxml.entity.HotelCharacteristic;
import com.epam.javaxml.entity.HotelIncludedVoucher;
import com.epam.javaxml.entity.TransportIncludedVoucher;
import com.epam.javaxml.entity.Voucher;
import com.epam.javaxml.parser.helper.VoucherEnum;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

// class to parse xml file using StAX parser
public class TouristVouchersStAXBuilder extends AbstractTouristVouchersBuilder {
    private XMLInputFactory inputFactory;

    TouristVouchersStAXBuilder() {
        this(new HashSet<Voucher>());
    }

    private TouristVouchersStAXBuilder(Set<Voucher> touristVouchers) {
        super(touristVouchers);
        // configuring parser
        inputFactory = XMLInputFactory.newInstance();
    }

    @Override
    public Set<Voucher> getTouristVouchers() {
        return touristVouchers;
    }

    // method to build set of tourist vouchers
    @Override
    public void buildSetTouristVouchers(String xmlPath) {
        FileInputStream fileInputStream = null;
        XMLStreamReader xmlStreamReader = null;
        String name;
        try {
            fileInputStream = new FileInputStream(new File(xmlPath));
            xmlStreamReader = inputFactory.createXMLStreamReader(fileInputStream);
            // parsing xml file while it has elements
            while (xmlStreamReader.hasNext()) {
                int type = xmlStreamReader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = xmlStreamReader.getLocalName();
                    Voucher voucher = new Voucher();
                    if (VoucherEnum.valueOf(name.toUpperCase()) == VoucherEnum.HOTELINCLUDEDVOUCHER) {
                        voucher = buildVoucher("hotelIncludedVoucher", xmlStreamReader);
                    } else if (VoucherEnum.valueOf(name.toUpperCase()) == VoucherEnum.TRANSPORTINCLUDEDVOUCHER) {
                        voucher = buildVoucher("transportIncludedVoucher", xmlStreamReader);
                    }
                    touristVouchers.add(voucher);
                }
            }
            LOG.info("File " + xmlPath + " parsed successfully using StAX parser");
        } catch (FileNotFoundException e) {
            LOG.error("File " + xmlPath + " not found! " + e);
        } catch (XMLStreamException e) {
            LOG.error("StAX parsing error: " + e.getMessage());
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // method to parse Voucher object from stream
    private Voucher buildVoucher(String voucherKind, XMLStreamReader reader) throws XMLStreamException {
        Voucher voucher = null;
        if (voucherKind.equals("hotelIncludedVoucher")) {
            voucher = new HotelIncludedVoucher();
        } else if (voucherKind.equals("transportIncludedVoucher")) {
            voucher = new TransportIncludedVoucher();
        }
        assert voucher != null;
        // setting xml attributes to Voucher
        voucher.setId(reader.getAttributeValue(null, VoucherEnum.ID.getValue()));
        voucher.setVoucherType(reader.getAttributeValue(null, VoucherEnum.VOUCHERTYPE.getValue()));
        // setting xml elements to Voucher
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (VoucherEnum.valueOf(name.toUpperCase())) {
                        case COUNTRY:
                            voucher.setCountry(getXMLText(reader));
                            break;
                        case NUMBEROFDAYS:
                            voucher.setNumberOfDays(new BigInteger(getXMLText(reader)));
                            break;
                        case COST:
                            voucher.setCost(Double.parseDouble(getXMLText(reader)));
                            break;
                        case HOTELCHARACTERISTICS:
                            assert voucher instanceof HotelIncludedVoucher;
                            ((HotelIncludedVoucher) voucher).setHotelCharacteristics(getXMLHotelCharacteristic(reader));
                            break;
                        case TRANSPORT:
                            assert voucher instanceof TransportIncludedVoucher;
                            ((TransportIncludedVoucher) voucher).setTransport(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (VoucherEnum.valueOf(name.toUpperCase()) == VoucherEnum.HOTELINCLUDEDVOUCHER || VoucherEnum.valueOf(name.toUpperCase()) == VoucherEnum.TRANSPORTINCLUDEDVOUCHER) {
                        return voucher;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element");
    }

    //method to build HotelCharacteristic object
    private HotelCharacteristic getXMLHotelCharacteristic(XMLStreamReader reader) throws XMLStreamException {
        HotelCharacteristic hotelCharacteristic = new HotelCharacteristic();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (VoucherEnum.valueOf(name.toUpperCase())) {
                        case FOOD:
                            hotelCharacteristic.setFood(getXMLText(reader));
                            break;
                        case ROOMCAPACITY:
                            hotelCharacteristic.setRoomCapacity(new BigInteger(getXMLText(reader)));
                            break;
                        case NUMBEROFSTARS:
                            hotelCharacteristic.setNumberOfStars(new BigInteger(getXMLText(reader)));
                            break;
                        case ISTVPRESENT:
                            hotelCharacteristic.setIsTvPresent(Boolean.parseBoolean(getXMLText(reader)));
                            break;
                        case ISAIRCONDITIONERPRESENT:
                            hotelCharacteristic.setIsAirConditionerPresent(Boolean.parseBoolean(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (VoucherEnum.valueOf(name.toUpperCase()) == VoucherEnum.HOTELCHARACTERISTICS) {
                        return hotelCharacteristic;
                    }
                    break;

            }
        }
        throw new XMLStreamException("Unknown element in tag HotelCharacteristic");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
