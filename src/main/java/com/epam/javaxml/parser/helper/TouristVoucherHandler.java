package com.epam.javaxml.parser.helper;

import com.epam.javaxml.entity.HotelCharacteristic;
import com.epam.javaxml.entity.HotelIncludedVoucher;
import com.epam.javaxml.entity.TransportIncludedVoucher;
import com.epam.javaxml.entity.Voucher;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigInteger;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

// class used in SAX parser
public class TouristVoucherHandler extends DefaultHandler {
    private Set<Voucher> touristVouchers;
    private Voucher current = null;
    private VoucherEnum currentEnum = null;
    private EnumSet<VoucherEnum> withText;

    public TouristVoucherHandler() {
        touristVouchers = new HashSet<>();
        // setting range of xml elements
        withText = EnumSet.range(VoucherEnum.COUNTRY, VoucherEnum.ISAIRCONDITIONERPRESENT);
    }

    public Set<Voucher> getTouristVouchers() {
        return touristVouchers;
    }

    // parsing xnl element
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        // checking either it is beginning of voucher element itself or of it inner element
        if ("hotelIncludedVoucher".equals(localName)) {
            HotelCharacteristic hotelCharacteristic = new HotelCharacteristic();
            current = new HotelIncludedVoucher();
            ((HotelIncludedVoucher) current).setHotelCharacteristics(hotelCharacteristic);
        } else if ("transportIncludedVoucher".equals(localName)) {
            current = new TransportIncludedVoucher();
        } else {
            VoucherEnum temp = VoucherEnum.valueOf(localName.toUpperCase());
            if (withText.contains(temp)) {
                currentEnum = temp;
            }
        }
        // setting xml attributes to Voucher object
        if ("hotelIncludedVoucher".equals(localName) || "transportIncludedVoucher".equals(localName)) {
            current.setId(attributes.getValue(0));
            if (attributes.getLength() == 2) {
                current.setVoucherType(attributes.getValue(1));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if ("hotelIncludedVoucher".equals(localName) || "transportIncludedVoucher".equals(localName)) {
            touristVouchers.add(current);
        }
    }

    // setting xml elements to Voucher object
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        String tempString = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case COUNTRY:
                    current.setCountry(tempString);
                    break;
                case NUMBEROFDAYS:
                    current.setNumberOfDays(new BigInteger(tempString));
                    break;
                case COST:
                    current.setCost(Double.parseDouble(tempString));
                    break;
                case TRANSPORT:
                    assert current instanceof TransportIncludedVoucher;
                    ((TransportIncludedVoucher) current).setTransport(tempString);
                    break;
                case NUMBEROFSTARS:
                    assert current instanceof HotelIncludedVoucher;
                    ((HotelIncludedVoucher) current).getHotelCharacteristics().setNumberOfStars(new BigInteger(tempString));
                    break;
                case FOOD:
                    assert current instanceof HotelIncludedVoucher;
                    ((HotelIncludedVoucher) current).getHotelCharacteristics().setFood(tempString);
                    break;
                case ROOMCAPACITY:
                    assert current instanceof HotelIncludedVoucher;
                    ((HotelIncludedVoucher) current).getHotelCharacteristics().setRoomCapacity(new BigInteger(tempString));
                    break;
                case ISTVPRESENT:
                    assert current instanceof HotelIncludedVoucher;
                    ((HotelIncludedVoucher) current).getHotelCharacteristics().setIsTvPresent(Boolean.parseBoolean(tempString));
                    break;
                case ISAIRCONDITIONERPRESENT:
                    assert current instanceof HotelIncludedVoucher;
                    ((HotelIncludedVoucher) current).getHotelCharacteristics().setIsAirConditionerPresent(Boolean.parseBoolean(tempString));
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }
}
