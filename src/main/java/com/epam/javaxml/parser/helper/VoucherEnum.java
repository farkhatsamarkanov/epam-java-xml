package com.epam.javaxml.parser.helper;

public enum VoucherEnum {
    TOURISTVOUCHERS("touristVouchers"),
    HOTELINCLUDEDVOUCHER("hotelIncludedVoucher"),
    TRANSPORTINCLUDEDVOUCHER("transportIncludedVoucher"),
    ID("id"),
    VOUCHERTYPE("voucherType"),
    COUNTRY("country"),
    NUMBEROFDAYS("numberOfDays"),
    COST("cost"),
    TRANSPORT("transport"),
    NUMBEROFSTARS("numberOfStars"),
    FOOD("food"),
    ROOMCAPACITY("roomCapacity"),
    ISTVPRESENT("isTvPresent"),
    ISAIRCONDITIONERPRESENT("isAirConditionerPresent"),
    HOTELCHARACTERISTICS("hotelCharacteristic");

    private String value;

    VoucherEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
