package com.epam.javaxml.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;

// class to handle errors during xml file validation
public class TouristVoucherErrorHandler extends DefaultHandler {
    private final static Logger LOG = LogManager.getLogger(TouristVoucherErrorHandler.class);

    TouristVoucherErrorHandler() throws IOException {
    }

    public void warning(SAXParseException e) {
        LOG.warn(getLineAdress(e) + " - " + e.getMessage());
    }

    public void error(SAXParseException e) {
        LOG.error(getLineAdress(e) + " - " + e.getMessage());
    }

    public void fatalError(SAXParseException e) {
        LOG.fatal(getLineAdress(e) + " - " + e.getMessage());
    }

    private String getLineAdress(SAXParseException e) {
        return e.getLineNumber() + " : " + e.getColumnNumber();
    }
}
