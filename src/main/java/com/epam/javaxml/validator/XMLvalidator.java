package com.epam.javaxml.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

// class to validate xml file against xsd schema
public class XMLvalidator {
    private final static Logger LOG = LogManager.getLogger(XMLvalidator.class);
    private String xmlPath;
    private String schemaPath;
    private TouristVoucherErrorHandler touristVoucherErrorHandler;

    public XMLvalidator(String xmlPath, String schemaPath) {
        this.xmlPath = xmlPath;
        this.schemaPath = schemaPath;
    }

    public boolean validateXML() {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        File schemaLocation = new File(schemaPath);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(xmlPath);
            touristVoucherErrorHandler = new TouristVoucherErrorHandler();
            validator.setErrorHandler(touristVoucherErrorHandler);
            validator.validate(source);
            LOG.debug("XML file is valid.");
            return true;
        } catch (SAXException e) {
            LOG.error("Validation of XML file is not valid because " + e.getMessage());
        } catch (IOException e) {
            LOG.error("XML file is not valid because " + e.getMessage());
        }
        return false;
    }
}
